## Projects using nextflow

### RNASeq

- [https://_https://gitlab.biologie.ens-lyon.fr/gylab/salmoninyeast](https://_https://gitlab.biologie.ens-lyon.fr/gylab/salmoninyeast)
- [https://github.com/LBMC/readthroughpombe](https://github.com/LBMC/readthroughpombe)
- [https://_https://gitlab.biologie.ens-lyon.fr/vvanoost/nextflow](https://_https://gitlab.biologie.ens-lyon.fr/vvanoost/nextflow)
- [https://gitlab.biologie.ens-lyon.fr/elabaron/HIV_project](https://gitlab.biologie.ens-lyon.fr/elabaron/HIV_project)

### single-cell RNA_-Seq

- [https://gitlab.com/LBMC_UMR5239/sbdm/mars-seq](https://gitlab.com/LBMC_UMR5239/sbdm/mars-seq)

### DNASeq

- [https://github.com/LBMC/ChrSexebelari](https://github.com/LBMC/ChrSexebelari)

### Chip-Seq

- [https://gitlab.biologie.ens-lyon.fr/Auboeuf/ChIP-seq](https://gitlab.biologie.ens-lyon.fr/Auboeuf/ChIP-seq)

